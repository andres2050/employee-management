package database

import (
	"errors"
	"time"

	mysqlErrors "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	// DBConnection database connection pointer
	DBConnection *gorm.DB
	// DBCredentials credentials to connect to mysql database
	// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
	DBCredentials = "root:secret@tcp(127.0.0.1:3306)/employee_management?charset=utf8&parseTime=True&loc=Local"
)

// Model basic fields used in all database model
type Model struct {
	ID        uint           `gorm:"primarykey" json:"id"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at,omitempty"`
}

// Connect initialize global database connection
func Connect() error {
	var err error
	DBConnection, err = gorm.Open(mysql.New(mysql.Config{
		DSN: DBCredentials,
	}), &gorm.Config{})

	return err
}

// CheckDuplicateEntry use to check if error is duplicate entry
func CheckDuplicateEntry(err error) bool {
	var mysqlErr *mysqlErrors.MySQLError
	return errors.As(err, &mysqlErr) && mysqlErr.Number == 1062
}
