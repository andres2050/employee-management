package database

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConnect(t *testing.T) {
	c := require.New(t)

	err := Connect()
	c.Nil(err)

	DBCredentials = ""
	err = Connect()
	c.NotNil(err)
}
