package main

import (
	"gitlab.com/andres2050/employee-management/areas"
	"gitlab.com/andres2050/employee-management/countries"
	"gitlab.com/andres2050/employee-management/database"
	"gitlab.com/andres2050/employee-management/employees"
	identificationtypes "gitlab.com/andres2050/employee-management/identification-types"
)

func initializeIdentificationType() *[]identificationtypes.IdentificationType {
	return &[]identificationtypes.IdentificationType{
		{
			Name: "Cédula de Ciudadanía",
		},
		{
			Name: "Cédula de Extranjería",
		},
		{
			Name: "Pasaporte",
		},
		{
			Name: "Permiso Especial",
		},
	}
}

func initializeAreas() *[]areas.Area {
	return &[]areas.Area{
		{
			Name: "Administración",
		},
		{
			Name: "Financiera",
		},
		{
			Name: "Compras",
		},
		{
			Name: "Infraestructura",
		},
		{
			Name: "Operación",
		},
		{
			Name: "Talento Humano",
		},
		{
			Name: "Servicios Varios",
		},
		{
			Name: "Ingeniería",
		},
	}
}

func initializeCountries() *[]countries.Country {
	return &[]countries.Country{
		{
			Name:   "Colombia",
			Domain: "cidenet.com.co",
		},
		{
			Name:   "Estados Unidos",
			Domain: "cidenet.com.us",
		},
	}
}

// This package is used to recreate the database with its initial values.
func main() {
	err := database.Connect()
	if err != nil {
		panic(err)
	}

	err = database.DBConnection.AutoMigrate(&areas.Area{}, &countries.Country{},
		&identificationtypes.IdentificationType{}, &employees.Employee{})
	if err != nil {
		panic(err)
	}

	database.DBConnection.Create(initializeAreas())
	database.DBConnection.Create(initializeIdentificationType())
	database.DBConnection.Create(initializeCountries())
}
