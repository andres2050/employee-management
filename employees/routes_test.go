package employees

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"gorm.io/gorm"
)

func TestInitializeRoutes(t *testing.T) {
	var router = mux.NewRouter()
	apiRouter := router.PathPrefix("/api").Subrouter()
	InitializeRoutes(apiRouter)
}

func TestGetAll(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/employees", getAll).Methods("GET")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Correct request
	r := httptest.NewRequest("GET", "/employees", nil)

	// Test Status Internal Server Error
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)
	c.Panics(func() {
		response := executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK
	employeesMock := getEmployeesMock()
	rows := sqlmock.NewRows(getFieldsJSON(employeesMock[0])).
		AddRow(getValues(employeesMock[0])...).
		AddRow(getValues(employeesMock[1])...)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	response := executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestQuery(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/employees/{id}", query).Methods("GET")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Test Status Bad Request
	r := httptest.NewRequest("GET", "/employees/a", nil)

	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request
	r = httptest.NewRequest("GET", "/employees/1", nil)

	// Test Status Not Found
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	response = executeRequest(router, r)
	c.Equal(http.StatusNotFound, response.Code)

	// Test Status Internal Server Error
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)
	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK
	employeesMock := getEmployeesMock()
	rows := sqlmock.NewRows(getFieldsJSON(employeesMock[0])).
		AddRow(getValues(employeesMock[0])...)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestPost(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/employees", post).Methods("POST")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	r := httptest.NewRequest("POST", "/employees", nil)
	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Normalize Fails
	employeeMock := getEmployeesMock()[0]
	employeeMock.Identification = "```"
	body, err := json.Marshal(employeeMock)
	c.Nil(err)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Required Fields Fails
	employeeMock = getEmployeesMock()[0]
	employeeMock.FirstName = ""
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Required Fields Fails Date
	employeeMock = getEmployeesMock()[0]
	employeeMock.DateOfAdmission = time.Time{}
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Foreign Fail
	employeeMock = getEmployeesMock()[0]
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Foreign Fail
	employeeMock = getEmployeesMock()[0]
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	rows := sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(employeeMock.Country.ID, employeeMock.Country.Name, employeeMock.Country.Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Foreign Fail
	employeeMock = getEmployeesMock()[0]
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	rows = sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(employeeMock.Country.ID, employeeMock.Country.Name, employeeMock.Country.Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	rows2 := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.Area.ID, employeeMock.Area.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows2)
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request Internal Server
	employeeMock = getEmployeesMock()[0]
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	rows = sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(employeeMock.Country.ID, employeeMock.Country.Name, employeeMock.Country.Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	rows2 = sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.Area.ID, employeeMock.Area.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows2)

	rows3 := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.IdentificationType.ID, employeeMock.IdentificationType.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows3)

	mock.ExpectExec("^INSERT (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Correct request ok
	employeeMock = getEmployeesMock()[0]
	body, err = json.Marshal(employeeMock)
	c.Nil(err)

	rows = sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(employeeMock.Country.ID, employeeMock.Country.Name, employeeMock.Country.Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	rows2 = sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.Area.ID, employeeMock.Area.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows2)

	rows3 = sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.IdentificationType.ID, employeeMock.IdentificationType.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows3)

	employeesMock := getEmployeesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectCommit()

	r = httptest.NewRequest("POST", "/employees", bytes.NewReader(body))

	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestPut(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/employees/{id}", put).Methods("PUT")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Test Status Bad Request
	r := httptest.NewRequest("PUT", "/employees/a", nil)

	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request
	r = httptest.NewRequest("PUT", "/employees/1", nil)

	// Test Status Not Found
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	response = executeRequest(router, r)
	c.Equal(http.StatusNotFound, response.Code)

	// Test Status Internal Server Error
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)
	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status Bad Request
	employeesMock := getEmployeesMock()
	rows := sqlmock.NewRows(getFieldsJSON(employeesMock[0])).
		AddRow(getValues(employeesMock[0])...)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request ok
	employeeMock := getEmployeesMock()[0]
	body, err := json.Marshal(employeeMock)
	c.Nil(err)

	rows = sqlmock.NewRows(getFieldsJSON(employeesMock[0])).
		AddRow(getValues(employeesMock[0])...)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	rows1 := sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(employeeMock.Country.ID, employeeMock.Country.Name, employeeMock.Country.Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows1)

	rows2 := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.Area.ID, employeeMock.Area.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows2)

	rows3 := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(employeeMock.IdentificationType.ID, employeeMock.IdentificationType.Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows3)

	employeesMock = getEmployeesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectCommit()

	r = httptest.NewRequest("PUT", "/employees/1", bytes.NewReader(body))

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})
}

func TestDelete(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/areas/{id}", delete).Methods("DELETE")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Correct request
	areasMock := getEmployeesMock()

	// Test Status Bad Request params
	r := httptest.NewRequest("DELETE", "/areas/a", nil)

	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Test Status Not Found Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	response = executeRequest(router, r)
	c.Equal(http.StatusNotFound, response.Code)

	// Test Status Internal Server Error Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status Internal Server Error Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status Internal Server Error Remove
	rows := sqlmock.NewRows([]string{"id"}).
		AddRow(areasMock[0].ID)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectExec("^UPDATE (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK Update
	rows = sqlmock.NewRows([]string{"id"}).
		AddRow(areasMock[0].ID)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	r = httptest.NewRequest("DELETE", "/areas/1", nil)
	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func executeRequest(router *mux.Router, req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr
}
