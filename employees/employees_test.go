package employees

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"gitlab.com/andres2050/employee-management/areas"
	"gitlab.com/andres2050/employee-management/countries"
	"gitlab.com/andres2050/employee-management/database"
	identificationtypes "gitlab.com/andres2050/employee-management/identification-types"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestList(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	employeesMock := getEmployeesMock()
	rows := sqlmock.NewRows(getFieldsJSON(employeesMock[0])).
		AddRow(getValues(employeesMock[0])...).
		AddRow(getValues(employeesMock[1])...)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	employees, err := list(context.Background())
	c.Nil(err)
	c.Len(employees, 2)
}

func TestFind(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	employeesMock := getEmployeesMock()
	rows := sqlmock.NewRows(getFieldsJSON(employeesMock[0])).
		AddRow(getValues(employeesMock[0])...)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	area, err := find(context.Background(), 1)
	c.Nil(err)

	c.Equal(employeesMock[0].Email, area.Email)
}

func TestCreate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	employeesMock := getEmployeesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectCommit()

	area := Employee{
		FirstName: employeesMock[0].FirstName,
	}
	err = create(context.Background(), &area)
	c.Nil(err)

	c.Equal(employeesMock[0].FirstName, area.FirstName)
	c.Equal(employeesMock[0].ID, area.ID)
}

func TestUpdate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	employeesMock := getEmployeesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].CountryID), 1))
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].AreaID), 1))
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].IdentificationTypeID), 1))
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectCommit()

	area := employeesMock[0]
	err = update(context.Background(), &area, employeesMock[1])
	c.Nil(err)

	c.Equal(employeesMock[1].FirstName, area.FirstName)
	c.Equal(employeesMock[1].ID, area.ID)
}

func TestRemove(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	employeesMock := getEmployeesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(employeesMock[0].ID), 1))
	mock.ExpectCommit()

	area := employeesMock[0]
	err = remove(context.Background(), &area)
	c.Nil(err)
}

func TestGetEmail(t *testing.T) {
	c := require.New(t)

	employeesMock := getEmployeesMock()
	employeeMock := employeesMock[0]
	err := employeeMock.getEmail(context.Background())
	c.Nil(err)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	employeeMock.Email = ""
	err = employeeMock.getEmail(context.Background())
	c.NotNil(err)

	rows := sqlmock.NewRows([]string{"email"})
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	err = employeeMock.getEmail(context.Background())
	c.Nil(err)

	employeeMock.Email = ""
	rows = sqlmock.NewRows([]string{"email"}).
		AddRow(employeesMock[0].Email).
		AddRow(employeesMock[1].Email)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	err = employeeMock.getEmail(context.Background())
	c.Nil(err)
}

func getEmployeesMock() []Employee {
	return []Employee{
		{
			Model:                database.Model{ID: uint(1)},
			FirstName:            "prueba",
			OtherNames:           "",
			Surname:              "test",
			SecondSurname:        "",
			Email:                "prueba.test@test",
			CountryID:            1,
			Country:              &countries.Country{Model: database.Model{ID: uint(1)}, Name: "TEST", Domain: "test"},
			DateOfAdmission:      time.Now(),
			AreaID:               1,
			Area:                 &areas.Area{Model: database.Model{ID: uint(1)}, Name: "TEST"},
			Identification:       "123456789",
			IdentificationTypeID: 1,
			IdentificationType:   &identificationtypes.IdentificationType{Model: database.Model{ID: uint(1)}, Name: "TEST"},
		},
		{
			Model:                database.Model{ID: uint(2)},
			FirstName:            "prueba",
			OtherNames:           "",
			Surname:              "test",
			SecondSurname:        "",
			Email:                "prueba.test.1@test",
			CountryID:            1,
			Country:              &countries.Country{Model: database.Model{ID: uint(1)}, Name: "TEST", Domain: "test"},
			DateOfAdmission:      time.Now(),
			AreaID:               1,
			Area:                 &areas.Area{Model: database.Model{ID: uint(1)}, Name: "TEST"},
			Identification:       "987654321",
			IdentificationTypeID: 1,
			IdentificationType:   &identificationtypes.IdentificationType{Model: database.Model{ID: uint(1)}, Name: "TEST"},
		},
	}
}

func TestGETJSON(t *testing.T) {
	v, _ := json.Marshal(getEmployeesMock()[0])
	fmt.Println(string(v))
}

func databaseMock() (*sql.DB, sqlmock.Sqlmock, error) {
	sqlDB, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      sqlDB,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	database.DBConnection = gormDB

	return sqlDB, mock, nil
}

func getFieldsJSON(e Employee) []string {
	val := reflect.ValueOf(e)
	fields := []string{}

	for i := 0; i < val.Type().NumField(); i++ {
		if !omitForeign(val.Type().Field(i).Tag.Get("json")) {
			fields = append(fields, val.Type().Field(i).Tag.Get("json"))
		}
	}

	return fields
}

func getValues(e Employee) []driver.Value {
	val := reflect.ValueOf(e)
	values := []driver.Value{}

	for i := 0; i < val.Type().NumField(); i++ {
		if !omitForeign(val.Type().Field(i).Tag.Get("json")) {
			values = append(values, val.Field(i).Interface())
		}
	}

	return values
}

func omitForeign(key string) bool {
	foreign := []string{"", "area", "country", "identification_type"}
	for _, f := range foreign {
		if key == f {
			return true
		}
	}

	return false
}
