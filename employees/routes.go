package employees

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/andres2050/employee-management/api"
	"gitlab.com/andres2050/employee-management/database"
	"gorm.io/gorm"
)

// InitializeRoutes Define routes of Countries endpoint.
func InitializeRoutes(router *mux.Router) {
	employees := router.PathPrefix("/employees").Subrouter()

	employees.HandleFunc("", getAll).Methods("GET")
	employees.HandleFunc("/{id}", query).Methods("GET")
	employees.HandleFunc("", post).Methods("POST")
	employees.HandleFunc("/{id}", put).Methods("PUT")
	employees.HandleFunc("/{id}", delete).Methods("DELETE")
}

func getAll(w http.ResponseWriter, r *http.Request) {
	employees, err := list(r.Context())
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, employees)
}

func query(w http.ResponseWriter, r *http.Request) {
	employee, internal, err := proccessID(w, r)
	if err != nil {
		if !internal {
			return
		}

		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, employee)
}

func post(w http.ResponseWriter, r *http.Request) {
	employee, internal, err := proccessBody(w, r)
	if err != nil {
		if !internal {
			return
		}

		panic(err)
	}

	err = employee.getEmail(r.Context())
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = create(r.Context(), &employee)
	if err != nil {
		if database.CheckDuplicateEntry(err) {
			api.RespondWithError(w, http.StatusBadRequest, "the combination of type and identification number already exists")
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, employee)
}

func put(w http.ResponseWriter, r *http.Request) {
	employee, internal, err := proccessID(w, r)
	if err != nil {
		if !internal {
			return
		}

		panic(err)
	}

	request, internal, err := proccessBody(w, r)
	if err != nil {
		if !internal {
			return
		}

		panic(err)
	}

	request.Email = employee.Email

	err = request.getEmail(r.Context())
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = update(r.Context(), &employee, request)
	if err != nil {
		if database.CheckDuplicateEntry(err) {
			api.RespondWithError(w, http.StatusBadRequest, "the combination of type and identification number already exists")
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, employee)
}

func delete(w http.ResponseWriter, r *http.Request) {
	employee, internal, err := proccessID(w, r)
	if err != nil {
		if !internal {
			return
		}

		panic(err)
	}

	err = remove(r.Context(), &employee)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, employee)
}

func proccessBody(w http.ResponseWriter, r *http.Request) (Employee, bool, error) {
	employee := Employee{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err := d.Decode(&employee)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return employee, false, err
	}

	err = employee.normalizeFields()
	if err != nil {
		if errors.Is(err, ErrContainsSpecialCharacters) || errors.Is(err, ErrInvalidIdentification) {
			api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("%v", err))
			return employee, false, err
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")

		return employee, true, err
	}

	err = employee.checkRequiredFields(r.Context())
	if err != nil {
		if errors.Is(err, ErrBadRequest) {
			api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("%v", err))
			return employee, false, err
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")

		return employee, true, err
	}

	return employee, false, nil
}

func proccessID(w http.ResponseWriter, r *http.Request) (Employee, bool, error) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return Employee{}, false, err
	}

	employee, err := find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return employee, false, err
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")

		return employee, true, err
	}

	return employee, false, nil
}
