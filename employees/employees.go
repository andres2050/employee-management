package employees

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"
	"unicode"

	"gitlab.com/andres2050/employee-management/areas"
	"gitlab.com/andres2050/employee-management/countries"
	"gitlab.com/andres2050/employee-management/database"
	identificationtypes "gitlab.com/andres2050/employee-management/identification-types"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"gorm.io/gorm"
)

var (
	// ErrBadRequest error to mark errors in checkRequiredFields
	ErrBadRequest = errors.New("bad request")
	// ErrContainsSpecialCharacters error to indicate that a field contains special characters
	ErrContainsSpecialCharacters = errors.New("fields contains special characters")
	// ErrInvalidIdentification error to indicate invalid identification
	ErrInvalidIdentification = errors.New("invalid identification number")
)

// Employee struct of database table
type Employee struct {
	database.Model
	FirstName            string                                  `gorm:"size:20;not null" json:"first_name"`
	OtherNames           string                                  `gorm:"size:50" json:"other_names"`
	Surname              string                                  `gorm:"size:20;not null" json:"surname"`
	SecondSurname        string                                  `gorm:"size:20" json:"second_surname"`
	Email                string                                  `gorm:"unique;size:300" json:"email"`
	CountryID            int                                     `gorm:"not null" json:"country_id"`
	Country              *countries.Country                      `json:"country"`
	DateOfAdmission      time.Time                               `gorm:"not null" json:"date_of_admission"`
	AreaID               int                                     `gorm:"not null" json:"area_id"`
	Area                 *areas.Area                             `json:"area"`
	Identification       string                                  `gorm:"size:20;not null;uniqueIndex:udx_identification" json:"identification"`
	IdentificationTypeID int                                     `gorm:"not null;uniqueIndex:udx_identification" json:"identification_type_id"`
	IdentificationType   *identificationtypes.IdentificationType `json:"identification_type"`
}

// BeforeDelete remove the email before of delete action
func (e *Employee) BeforeDelete(tx *gorm.DB) (err error) {
	e.Email = ""

	return tx.Model(e).Select("email").Updates(e).Error
}

func (e *Employee) getArea(ctx context.Context) error {
	area, err := areas.Find(ctx, int64(e.AreaID))
	e.Area = &area

	return err
}

func (e *Employee) getIdentificationType(ctx context.Context) error {
	identificationType, err := identificationtypes.Find(ctx, int64(e.IdentificationTypeID))
	e.IdentificationType = &identificationType

	return err
}

func (e *Employee) getCountry(ctx context.Context) error {
	country, err := countries.Find(ctx, int64(e.CountryID))
	e.Country = &country

	return err
}

func list(ctx context.Context) ([]Employee, error) {
	areas := []Employee{}
	err := database.DBConnection.WithContext(ctx).
		Joins("Country").
		Joins("Area").
		Joins("IdentificationType").
		Find(&areas).Error

	return areas, err
}

func find(ctx context.Context, id int64) (Employee, error) {
	area := Employee{}
	err := database.DBConnection.WithContext(ctx).
		Joins("Country").
		Joins("Area").
		Joins("IdentificationType").
		First(&area, id).Error

	return area, err
}

func create(ctx context.Context, r *Employee) error {
	return database.DBConnection.WithContext(ctx).Create(r).Error
}

func update(ctx context.Context, area *Employee, changes Employee) error {
	return database.DBConnection.WithContext(ctx).Model(area).Updates(changes).Error
}

func remove(ctx context.Context, area *Employee) error {
	return database.DBConnection.WithContext(ctx).Model(&area).Delete(&area).Error
}

func (e *Employee) getEmail(ctx context.Context) error {
	checkEmail := regexp.MustCompile(strings.ToLower(fmt.Sprintf("%s.%s[.0-9]*@%s", e.FirstName, e.Surname, e.Country.Domain)))
	if checkEmail.MatchString(e.Email) {
		return nil
	}

	r := []Employee{}
	tempEmail := strings.ToLower(fmt.Sprintf("%s.%s%s@%s", e.FirstName, e.Surname, "%", e.Country.Domain))

	err := database.DBConnection.WithContext(ctx).
		Select("email").
		Find(&r, "email LIKE ?", tempEmail).Error
	if err != nil {
		return err
	}

	if len(r) == 0 {
		e.Email = strings.ToLower(fmt.Sprintf("%s.%s@%s", e.FirstName, e.Surname, e.Country.Domain))
		return nil
	}

	e.findAvailableEmail(r)

	return nil
}

func (e *Employee) getForeignValues(ctx context.Context) error {
	err := e.getCountry(ctx)
	if err != nil {
		return err
	}

	err = e.getArea(ctx)
	if err != nil {
		return err
	}

	err = e.getIdentificationType(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (e *Employee) checkRequiredFields(ctx context.Context) error {
	if e.FirstName == "" ||
		e.Identification == "" ||
		e.Surname == "" {
		return ErrBadRequest
	}

	if e.DateOfAdmission.IsZero() || e.DateOfAdmission.After(time.Now()) {
		return ErrBadRequest
	}

	err := e.getForeignValues(ctx)
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return ErrBadRequest
	}

	return err
}

func (e *Employee) findAvailableEmail(emails []Employee) {
	emailIndex := 0
	email := strings.ToLower(fmt.Sprintf("%s.%s@%s", e.FirstName, e.Surname, e.Country.Domain))

	for {
		exist := false

		for _, v := range emails {
			if v.Email == email {
				exist = true
				break
			}
		}

		if !exist {
			e.Email = email
			return
		}
		emailIndex++
		email = strings.ToLower(fmt.Sprintf("%s.%s.%d@%s", e.FirstName, e.Surname, emailIndex, e.Country.Domain))
	}
}

func (e *Employee) normalizeFields() error {
	var err error

	e.FirstName, err = normalizeString(e.FirstName)
	if err != nil {
		return err
	}

	e.OtherNames, err = normalizeString(e.OtherNames)
	if err != nil {
		return err
	}

	e.Surname, err = normalizeString(e.Surname)
	if err != nil {
		return err
	}

	e.SecondSurname, err = normalizeString(e.SecondSurname)
	if err != nil {
		return err
	}

	isValidID := regexp.MustCompile(`[A-Za-z0-9\-]+$`).MatchString

	e.Identification = strings.TrimSpace(e.Identification)
	if !isValidID(e.Identification) {
		return ErrInvalidIdentification
	}

	return nil
}

func normalizeString(s string) (string, error) {
	s, err := removeAccents(strings.TrimSpace(s))
	if err != nil {
		return s, err
	}

	isAlpha := regexp.MustCompile(`^[A-Za-z ]*$`).MatchString
	if !isAlpha(s) {
		return s, ErrContainsSpecialCharacters
	}

	return strings.ToUpper(s), nil
}

func removeAccents(s string) (string, error) {
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)

	output, _, err := transform.String(t, s)
	if err != nil {
		return s, err
	}

	return output, nil
}
