package identificationtypes

import (
	"context"
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"gitlab.com/andres2050/employee-management/database"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestList(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	identificationTypesMock := getIdentificationTypesMock()
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(identificationTypesMock[0].ID, identificationTypesMock[0].Name).
		AddRow(identificationTypesMock[1].ID, identificationTypesMock[1].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	identificationTypes, err := list(context.Background())
	c.Nil(err)

	c.Equal(identificationTypesMock, identificationTypes)
}

func TestFind(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	identificationTypesMock := getIdentificationTypesMock()
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(identificationTypesMock[0].ID, identificationTypesMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	area, err := Find(context.Background(), 1)
	c.Nil(err)

	c.Equal(identificationTypesMock[0], area)
}

func TestCreate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	identificationTypesMock := getIdentificationTypesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(identificationTypesMock[0].ID), 1))
	mock.ExpectCommit()

	area := IdentificationType{
		Name: identificationTypesMock[0].Name,
	}
	err = create(context.Background(), &area)
	c.Nil(err)

	c.Equal(identificationTypesMock[0].Name, area.Name)
	c.Equal(identificationTypesMock[0].ID, area.ID)
}

func TestUpdate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	identificationTypesMock := getIdentificationTypesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(identificationTypesMock[0].ID), 1))
	mock.ExpectCommit()

	area := identificationTypesMock[0]
	err = update(context.Background(), &area, identificationTypesMock[1])
	c.Nil(err)

	c.Equal(identificationTypesMock[1].Name, area.Name)
	c.Equal(identificationTypesMock[1].ID, area.ID)
}

func TestRemove(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	identificationTypesMock := getIdentificationTypesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(identificationTypesMock[0].ID), 1))
	mock.ExpectCommit()

	area := identificationTypesMock[0]
	err = remove(context.Background(), &area)
	c.Nil(err)
}

func getIdentificationTypesMock() []IdentificationType {
	return []IdentificationType{
		{
			Model: database.Model{
				ID: uint(1),
			},
			Name: "PRUEBA",
		},
		{
			Model: database.Model{
				ID: uint(2),
			},
			Name: "PRUEBA2",
		},
	}
}

func databaseMock() (*sql.DB, sqlmock.Sqlmock, error) {
	sqlDB, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      sqlDB,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	database.DBConnection = gormDB

	return sqlDB, mock, nil
}
