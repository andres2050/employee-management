package identificationtypes

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/andres2050/employee-management/api"
	"gorm.io/gorm"
)

// InitializeRoutes Define routes of IdentificationTypes endpoint.
func InitializeRoutes(router *mux.Router) {
	identificationTypes := router.PathPrefix("/identificationtypes").Subrouter()

	identificationTypes.HandleFunc("", getAll).Methods("GET")
	identificationTypes.HandleFunc("/{id}", query).Methods("GET")
	identificationTypes.HandleFunc("", post).Methods("POST")
	identificationTypes.HandleFunc("/{id}", put).Methods("PUT")
	identificationTypes.HandleFunc("/{id}", delete).Methods("DELETE")
}

func getAll(w http.ResponseWriter, r *http.Request) {
	identificationTypes, err := list(r.Context())
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, identificationTypes)
}

func query(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func post(w http.ResponseWriter, r *http.Request) {
	area := IdentificationType{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err := d.Decode(&area)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return
	}

	err = create(r.Context(), &area)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func put(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	request := IdentificationType{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err = d.Decode(&request)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = update(r.Context(), &area, request)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = remove(r.Context(), &area)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}
