package identificationtypes

import (
	"context"

	"gitlab.com/andres2050/employee-management/database"
)

// IdentificationType struct of database table
type IdentificationType struct {
	database.Model
	Name string `gorm:"unique;not null" json:"name"`
}

func list(ctx context.Context) ([]IdentificationType, error) {
	areas := []IdentificationType{}
	err := database.DBConnection.WithContext(ctx).Find(&areas).Error

	return areas, err
}

// Find get IdentificationType by ID
func Find(ctx context.Context, id int64) (IdentificationType, error) {
	area := IdentificationType{}
	err := database.DBConnection.WithContext(ctx).First(&area, id).Error

	return area, err
}

func create(ctx context.Context, r *IdentificationType) error {
	return database.DBConnection.WithContext(ctx).Create(r).Error
}

func update(ctx context.Context, area *IdentificationType, changes IdentificationType) error {
	return database.DBConnection.WithContext(ctx).Model(area).Updates(changes).Error
}

func remove(ctx context.Context, area *IdentificationType) error {
	return database.DBConnection.WithContext(ctx).Delete(&area).Error
}
