package api

import (
	"encoding/json"
	"net/http"
)

// Response struc use for api all api response
type Response struct {
	Message  string      `json:"message,omitempty"`
	Response interface{} `json:"response,omitempty"`
}

// RespondWithError return error using Response struct in Message field
func RespondWithError(w http.ResponseWriter, code int, message string) {
	response, _ := json.Marshal(Response{
		Message: message,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}

// RespondWithJSON return json using Response struct in Response field
func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(Response{
		Response: payload,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}
