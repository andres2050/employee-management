package areas

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"gorm.io/gorm"
)

func TestInitializeRoutes(t *testing.T) {
	var router = mux.NewRouter()
	apiRouter := router.PathPrefix("/api").Subrouter()
	InitializeRoutes(apiRouter)
}

func TestGetAll(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/areas", getAll).Methods("GET")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Correct request
	r := httptest.NewRequest("GET", "/areas", nil)

	// Test Status Internal Server Error
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)
	c.Panics(func() {
		response := executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK
	areasMock := getAreasMock()
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name).
		AddRow(areasMock[1].ID, areasMock[1].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	response := executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestQuery(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/areas/{id}", query).Methods("GET")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Test Status Bad Request
	r := httptest.NewRequest("GET", "/areas/a", nil)

	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request
	r = httptest.NewRequest("GET", "/areas/1", nil)

	// Test Status Not Found
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	response = executeRequest(router, r)
	c.Equal(http.StatusNotFound, response.Code)

	// Test Status Internal Server Error
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)
	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK
	areasMock := getAreasMock()
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestPost(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/areas", post).Methods("POST")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Test Status Bad Request
	badParams := map[string]interface{}{
		"test": "test",
	}
	badBody, err := json.Marshal(badParams)
	c.Nil(err)

	r := httptest.NewRequest("POST", "/areas", bytes.NewReader(badBody))
	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request
	areasMock := getAreasMock()
	body, err := json.Marshal(areasMock[0])
	c.Nil(err)

	r = httptest.NewRequest("POST", "/areas", bytes.NewReader(body))

	// Test Status Internal Server Error
	mock.ExpectExec("^INSERT (.*)").WillReturnError(gorm.ErrInvaildDB)
	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK
	mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	r = httptest.NewRequest("POST", "/areas", bytes.NewReader(body))

	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestPut(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/areas/{id}", put).Methods("PUT")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Test Status Bad Request params
	r := httptest.NewRequest("PUT", "/areas/a", nil)

	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Test Status Bad Request Body
	badParams := map[string]interface{}{
		"test": "test",
	}
	badBody, err := json.Marshal(badParams)
	c.Nil(err)

	r = httptest.NewRequest("PUT", "/areas/1", bytes.NewReader(badBody))
	response = executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Correct request
	areasMock := getAreasMock()
	body, err := json.Marshal(areasMock[0])
	c.Nil(err)

	// Test Status Not Found Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	r = httptest.NewRequest("PUT", "/areas/1", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusNotFound, response.Code)

	// Test Status Internal Server Error Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("PUT", "/areas/1", bytes.NewReader(body))

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status Internal Server Error Update
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectExec("^UPDATE (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("PUT", "/areas/1", bytes.NewReader(body))

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK Update
	rows = sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	r = httptest.NewRequest("PUT", "/areas/1", bytes.NewReader(body))
	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func TestDelete(t *testing.T) {
	c := require.New(t)

	var router = mux.NewRouter()
	router.HandleFunc("/areas/{id}", delete).Methods("DELETE")

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	// Correct request
	areasMock := getAreasMock()

	// Test Status Bad Request params
	r := httptest.NewRequest("DELETE", "/areas/a", nil)

	response := executeRequest(router, r)
	c.Equal(http.StatusBadRequest, response.Code)

	// Test Status Not Found Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrRecordNotFound)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	response = executeRequest(router, r)
	c.Equal(http.StatusNotFound, response.Code)

	// Test Status Internal Server Error Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status Internal Server Error Find
	mock.ExpectQuery("^SELECT (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status Internal Server Error Remove
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectExec("^UPDATE (.*)").WillReturnError(gorm.ErrInvaildDB)

	r = httptest.NewRequest("DELETE", "/areas/1", nil)

	c.Panics(func() {
		response = executeRequest(router, r)
		c.Equal(http.StatusInternalServerError, response.Code)
	})

	// Test Status OK Update
	rows = sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)
	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	r = httptest.NewRequest("DELETE", "/areas/1", nil)
	response = executeRequest(router, r)
	c.Equal(http.StatusOK, response.Code)
}

func executeRequest(router *mux.Router, req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	return rr
}
