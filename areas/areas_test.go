package areas

import (
	"context"
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"gitlab.com/andres2050/employee-management/database"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestList(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	areasMock := getAreasMock()
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name).
		AddRow(areasMock[1].ID, areasMock[1].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	areas, err := list(context.Background())
	c.Nil(err)

	c.Equal(areasMock, areas)
}

func TestFind(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	areasMock := getAreasMock()
	rows := sqlmock.NewRows([]string{"id", "name"}).
		AddRow(areasMock[0].ID, areasMock[0].Name)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	area, err := Find(context.Background(), 1)
	c.Nil(err)

	c.Equal(areasMock[0], area)
}

func TestCreate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	areasMock := getAreasMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	area := Area{
		Name: areasMock[0].Name,
	}
	err = create(context.Background(), &area)
	c.Nil(err)

	c.Equal(areasMock[0].Name, area.Name)
	c.Equal(areasMock[0].ID, area.ID)
}

func TestUpdate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	areasMock := getAreasMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	area := areasMock[0]
	err = update(context.Background(), &area, areasMock[1])
	c.Nil(err)

	c.Equal(areasMock[1].Name, area.Name)
	c.Equal(areasMock[1].ID, area.ID)
}

func TestRemove(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	areasMock := getAreasMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(areasMock[0].ID), 1))
	mock.ExpectCommit()

	area := areasMock[0]
	err = remove(context.Background(), &area)
	c.Nil(err)
}

func getAreasMock() []Area {
	return []Area{
		{
			Model: database.Model{
				ID: uint(1),
			},
			Name: "PRUEBA",
		},
		{
			Model: database.Model{
				ID: uint(2),
			},
			Name: "PRUEBA2",
		},
	}
}

func databaseMock() (*sql.DB, sqlmock.Sqlmock, error) {
	sqlDB, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      sqlDB,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	database.DBConnection = gormDB

	return sqlDB, mock, nil
}
