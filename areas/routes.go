package areas

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/andres2050/employee-management/api"
	"gorm.io/gorm"
)

// InitializeRoutes Define routes of Areas endpoint.
func InitializeRoutes(router *mux.Router) {
	areas := router.PathPrefix("/areas").Subrouter()

	areas.HandleFunc("", getAll).Methods("GET")
	areas.HandleFunc("/{id}", query).Methods("GET")
	areas.HandleFunc("", post).Methods("POST")
	areas.HandleFunc("/{id}", put).Methods("PUT")
	areas.HandleFunc("/{id}", delete).Methods("DELETE")
}

func getAll(w http.ResponseWriter, r *http.Request) {
	areas, err := list(r.Context())
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, areas)
}

func query(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func post(w http.ResponseWriter, r *http.Request) {
	area := Area{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err := d.Decode(&area)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return
	}

	err = create(r.Context(), &area)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func put(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	request := Area{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err = d.Decode(&request)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = update(r.Context(), &area, request)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = remove(r.Context(), &area)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}
