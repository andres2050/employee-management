package areas

import (
	"context"

	"gitlab.com/andres2050/employee-management/database"
)

// Area struct of database table
type Area struct {
	database.Model
	Name string `gorm:"unique;not null" json:"name"`
}

func list(ctx context.Context) ([]Area, error) {
	areas := []Area{}
	err := database.DBConnection.WithContext(ctx).Find(&areas).Error

	return areas, err
}

// Find get Area by ID
func Find(ctx context.Context, id int64) (Area, error) {
	area := Area{}
	err := database.DBConnection.WithContext(ctx).First(&area, id).Error

	return area, err
}

func create(ctx context.Context, r *Area) error {
	return database.DBConnection.WithContext(ctx).Create(r).Error
}

func update(ctx context.Context, area *Area, changes Area) error {
	return database.DBConnection.WithContext(ctx).Model(area).Updates(changes).Error
}

func remove(ctx context.Context, area *Area) error {
	return database.DBConnection.WithContext(ctx).Delete(&area).Error
}
