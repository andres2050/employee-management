package countries

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/andres2050/employee-management/api"
	"gorm.io/gorm"
)

// InitializeRoutes Define routes of Countries endpoint.
func InitializeRoutes(router *mux.Router) {
	countries := router.PathPrefix("/countries").Subrouter()

	countries.HandleFunc("", getAll).Methods("GET")
	countries.HandleFunc("/{id}", query).Methods("GET")
	countries.HandleFunc("", post).Methods("POST")
	countries.HandleFunc("/{id}", put).Methods("PUT")
	countries.HandleFunc("/{id}", delete).Methods("DELETE")
}

func getAll(w http.ResponseWriter, r *http.Request) {
	countries, err := list(r.Context())
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, countries)
}

func query(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func post(w http.ResponseWriter, r *http.Request) {
	area := Country{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err := d.Decode(&area)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return
	}

	err = create(r.Context(), &area)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func put(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	request := Country{}
	d := json.NewDecoder(r.Body)
	d.DisallowUnknownFields()

	err = d.Decode(&request)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, "Bad Request")

		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = update(r.Context(), &area, request)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}

func delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		api.RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("Invalid ID: %v", mux.Vars(r)["id"]))
		return
	}

	area, err := Find(r.Context(), id)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			api.RespondWithError(w, http.StatusNotFound, fmt.Sprintf("%v ID: %v", err, id))
			return
		}

		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	err = remove(r.Context(), &area)
	if err != nil {
		api.RespondWithError(w, http.StatusInternalServerError, "Internal Server Error")
		panic(err)
	}

	api.RespondWithJSON(w, http.StatusOK, area)
}
