package countries

import (
	"context"
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"gitlab.com/andres2050/employee-management/database"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func TestList(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	countriesMock := getCountriesMock()
	rows := sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(countriesMock[0].ID, countriesMock[0].Name, countriesMock[0].Domain).
		AddRow(countriesMock[1].ID, countriesMock[1].Name, countriesMock[1].Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	countries, err := list(context.Background())
	c.Nil(err)

	c.Equal(countriesMock, countries)
}

func TestFind(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	countriesMock := getCountriesMock()
	rows := sqlmock.NewRows([]string{"id", "name", "domain"}).
		AddRow(countriesMock[0].ID, countriesMock[0].Name, countriesMock[0].Domain)
	mock.ExpectQuery("^SELECT (.*)").WillReturnRows(rows)

	area, err := Find(context.Background(), 1)
	c.Nil(err)

	c.Equal(countriesMock[0], area)
}

func TestCreate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	countriesMock := getCountriesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^INSERT (.*)").
		WillReturnResult(sqlmock.NewResult(int64(countriesMock[0].ID), 1))
	mock.ExpectCommit()

	area := Country{
		Name: countriesMock[0].Name,
	}
	err = create(context.Background(), &area)
	c.Nil(err)

	c.Equal(countriesMock[0].Name, area.Name)
	c.Equal(countriesMock[0].ID, area.ID)
}

func TestUpdate(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	countriesMock := getCountriesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(countriesMock[0].ID), 1))
	mock.ExpectCommit()

	area := countriesMock[0]
	err = update(context.Background(), &area, countriesMock[1])
	c.Nil(err)

	c.Equal(countriesMock[1].Name, area.Name)
	c.Equal(countriesMock[1].ID, area.ID)
}

func TestRemove(t *testing.T) {
	c := require.New(t)

	sqlDB, mock, err := databaseMock()
	c.Nil(err)

	defer func() { _ = sqlDB.Close() }()

	countriesMock := getCountriesMock()
	_ = mock.ExpectBegin()
	mock.ExpectExec("^UPDATE (.*)").
		WillReturnResult(sqlmock.NewResult(int64(countriesMock[0].ID), 1))
	mock.ExpectCommit()

	area := countriesMock[0]
	err = remove(context.Background(), &area)
	c.Nil(err)
}

func getCountriesMock() []Country {
	return []Country{
		{
			Model: database.Model{
				ID: uint(1),
			},
			Name:   "PRUEBA",
			Domain: "prueba.co",
		},
		{
			Model: database.Model{
				ID: uint(2),
			},
			Name:   "PRUEBA2",
			Domain: "prueba.us",
		},
	}
}

func databaseMock() (*sql.DB, sqlmock.Sqlmock, error) {
	sqlDB, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	gormDB, err := gorm.Open(mysql.New(mysql.Config{
		Conn:                      sqlDB,
		SkipInitializeWithVersion: true,
	}), &gorm.Config{})
	if err != nil {
		return nil, nil, err
	}

	database.DBConnection = gormDB

	return sqlDB, mock, nil
}
