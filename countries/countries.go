package countries

import (
	"context"

	"gitlab.com/andres2050/employee-management/database"
)

// Country struct of database table
type Country struct {
	database.Model
	Name   string `gorm:"unique;not null" json:"name"`
	Domain string `gorm:"unique;not null" json:"domain"`
}

func list(ctx context.Context) ([]Country, error) {
	countries := []Country{}
	err := database.DBConnection.WithContext(ctx).Find(&countries).Error

	return countries, err
}

// Find get Area by ID
func Find(ctx context.Context, id int64) (Country, error) {
	country := Country{}
	err := database.DBConnection.WithContext(ctx).First(&country, id).Error

	return country, err
}

func create(ctx context.Context, r *Country) error {
	return database.DBConnection.WithContext(ctx).Create(r).Error
}

func update(ctx context.Context, country *Country, changes Country) error {
	return database.DBConnection.WithContext(ctx).Model(country).Updates(changes).Error
}

func remove(ctx context.Context, country *Country) error {
	return database.DBConnection.WithContext(ctx).Delete(&country).Error
}
