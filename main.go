package main

import (
	stdlog "log"
	"os"

	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
	admissioncontrol "gitlab.com/andres2050/employee-management/admission-control"
	"gitlab.com/andres2050/employee-management/areas"
	"gitlab.com/andres2050/employee-management/countries"
	"gitlab.com/andres2050/employee-management/database"
	"gitlab.com/andres2050/employee-management/employees"
	identificationtypes "gitlab.com/andres2050/employee-management/identification-types"
)

func main() {
	err := database.Connect()
	if err != nil {
		panic(err)
	}

	r := mux.NewRouter()
	apiRouter := r.PathPrefix("/api").Subrouter()

	// Define routes
	areas.InitializeRoutes(apiRouter)
	countries.InitializeRoutes(apiRouter)
	identificationtypes.InitializeRoutes(apiRouter)
	employees.InitializeRoutes(apiRouter)

	var logger log.Logger
	// Logfmt is a structured, key=val logging format that is easy to read and parse
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	// Direct any attempts to use Go's log package to our structured logger
	stdlog.SetOutput(log.NewStdlibAdapter(logger))
	// Log the timestamp (in UTC) and the callsite (file + line number) of the logging
	// call for debugging in the future.
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "loc", log.DefaultCaller)

	// Create an instance of our LoggingMiddleware with our configured logger
	loggingMiddleware := admissioncontrol.LoggingMiddleware(logger)
	loggedRouter := loggingMiddleware(r)

	err = fasthttp.ListenAndServe(":8082", fasthttpadaptor.NewFastHTTPHandler(loggedRouter))
	if err != nil {
		panic(err)
	}
}
