module gitlab.com/andres2050/employee-management

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-kit/kit v0.10.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.22.0
	golang.org/x/text v0.3.6
	gorm.io/driver/mysql v1.0.5
	gorm.io/gorm v1.21.6
)
