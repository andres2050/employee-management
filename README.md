# Employee Management

Requisitos:

- Mysql 15.1
- Golang 1.16

Configurar base de datos:

Modificar la variable DBCredentials en el archivo database/database.go

Desplegar base de datos:

go run database/migrate/main.go

Ejecutar backend:

go run main.go