package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/andres2050/employee-management/database"
)

func TestMain(t *testing.T) {
	c := require.New(t)

	c.NotPanics(func() { main() })

	database.DBCredentials = ""

	c.Panics(func() { main() })
}
